@extends('layouts.template')

<!-- title -->
@section('title','Daftar Barang') @section('breadcrumb','Data Barang')

<!-- content -->
@section('content')

<section class="section">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-end">
                <a href="/barang/add" class="btn btn-primary">Tambah</a>
            </div>
            
            @include('components.alert')
            <table class="table table-striped" id="table1">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Stok</th>
                        <th>Keterangan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @php $number = 1 @endphp @foreach($barang as $barang)
                    <tr>
                        <td>{{$number++}}</td>
                        <td>{{$barang->name}}</td>
                        <td>{{$barang->stock}}</td>
                        <td>{{$barang->notes}}</td>
                        <td>
                            <a
                                href="/barang/edit/{{$barang->id}}"
                                class="btn btn-sm btn-success"
                                >Edit</a
                            >
                            <a
                                href="/barang/delete/{{$barang->id}}"
                                class="btn btn-sm btn-danger dlt"
                                >Hapus</a
                            >
                        </td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>

<script src="assets/js/jquery-3.6.3.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(document).ready(function () {
        $(".dlt").on("click", function () {
            console.log("jalan");
            Swal.fire({
                title: "Perhatian",
                text: "Apakah anda yakin ingin menghapus data ini ? ",
                icon: "question",
                showCancelButton: true,
                cancelButtonText: "Tidak",
                coonfirmButtonText: "Ya",
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location = "/barang/delete/{{$barang->id}}"
                } 
            });
        });
    });
</script>

@endsection
