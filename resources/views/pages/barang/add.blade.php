@extends('layouts.template')

<!-- title -->
@section('title','Tambah Barang')
<!-- breadcrumb -->
@section('breadcrumb','Tambah Barang')

<!-- content -->
@section('content')
<div class="col-md-12 col-12">
    @include('components.alert')
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <form
                    class="form form-vertical"
                    method="post"
                    action="{{ route('store-barang') }}"
                >
                    @csrf
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="first-name-vertical"
                                        >Nama Barang
                                        <small class="text-danger"
                                            >*</small
                                        ></label
                                    >
                                    <input
                                        type="text"
                                        id="first-name-vertical"
                                        class="form-control @error('name') is-invalid @enderror "
                                        name="name"
                                        placeholder="Nama Barang"
                                        value="{{old(('name'))}}"
                                    />
                                    @error('name')
                                    <small class="text-danger mt-2">
                                        {{ $message }}
                                    </small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="first-name-vertical"
                                        >Stok
                                        <small class="text-danger"
                                            >*</small
                                        ></label
                                    >
                                    <input
                                        type="text"
                                        id="first-name-vertical"
                                        class="form-control @error('stock') is-invalid @enderror"
                                        name="stock"
                                        placeholder="Stok Barang"
                                        value="{{old(('stock'))}}"
                                    />
                                    @error('stock')
                                    <small class="text-danger mt-2">
                                        {{ $message }}
                                    </small>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label for="first-name-vertical"
                                        >Keterangan</label
                                    >
                                    <textarea
                                        type="text"
                                        id="first-name-vertical"
                                        class="form-control"
                                        name="notes"
                                        value="{{old(('notes'))}}"
                                    >
                                    </textarea>
                                </div>
                            </div>

                            <div class="col-12 d-flex justify-content-end">
                                <a
                                    href="/barang"
                                    class="btn btn-light-secondary me-1 mb-1"
                                >
                                    Kembali
                                </a>
                                <button
                                    type="submit"
                                    class="btn btn-primary me-1 mb-1"
                                >
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
