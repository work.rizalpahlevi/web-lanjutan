@if(Session::get('result'))
<div class="alert alert-sm {{Session::get('type')}} alert-dismissible fade show mt-2" role="alert">
    {{Session::get('result')}}
    <button
        type="button"
        class="btn-close"
        data-bs-dismiss="alert"
        aria-label="Close"
    ></button>
</div>
@endif
