<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Data Barang</title>

        <link rel="stylesheet" href="{{ asset('assets/css/main/app.css')}}" />
        <link rel="stylesheet" href="{{ asset('assets/css/main/app-dark.css')}}" />
        <link
            rel="shortcut icon"
            href="{{ asset('assets/images/logo/favicon.svg')}}"
            type="image/x-icon"
        />
        <link
            rel="shortcut icon"
            href="{{ asset('assets/images/logo/favicon.png')}}"
            type="image/png"
        />

        <link rel="stylesheet" href="{{ asset('assets/css/shared/iconly.css')}}" />

        <!-- Datatable CSS -->
        <!-- <link
            rel="stylesheet"
            href="assets/vendors/simple-datatables/style.css"
        /> -->

        <link
            rel="stylesheet"
            href="assets/vendors/perfect-scrollbar/perfect-scrollbar.css"
        />
        <link
            rel="stylesheet"
            href="assets/vendors/bootstrap-icons/bootstrap-icons.css"
        />
        <link rel="stylesheet" href="assets/css/app.css" />
        <link
            rel="shortcut icon"
            href="assets/images/favicon.svg"
            type="image/x-icon"
        />
    </head>

    <body>
        <div id="app">
            @include('components.sidebar')
            <div id="main" class="layout-navbar">
                <header class="mb-3">
                    @include('components.head-profile')
                </header>
                <div id="main-content" style="padding-top: 0px;">
                    <div class="page-heading">
                        <div class="page-title">
                            <div class="row">
                                <div
                                    class="col-12 col-md-6 order-md-1 order-last"
                                >
                                    <h3>@yield('title')</h3>
                                </div>
                                <div
                                    class="col-12 col-md-6 order-md-2 order-first"
                                >
                                    <nav
                                        aria-label="breadcrumb"
                                        class="breadcrumb-header float-start float-lg-end"
                                    >
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.php"
                                                    >Master Data</a
                                                >
                                            </li>
                                            <li
                                                class="breadcrumb-item active"
                                                aria-current="page"
                                            >
                                                @yield('breadcrumb')
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    @yield('content')
                </div>
            </div>
        </div>

        <script src="assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendors/simple-datatables/simple-datatables.js"></script>
        <!--<script>
            // Datatable
            let table1 = document.querySelector("#table1");
            let dataTable = new simpleDatatables.DataTable(table1);
        </script> -->
        <script src="assets/js/mazer.js"></script>
    </body>
</html>
