<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Login - Mazer Admin Dashboard</title>
        <link rel="stylesheet" href="assets/css/main/app.css" />
        <link rel="stylesheet" href="assets/css/pages/auth.css" />
        <link
            rel="shortcut icon"
            href="assets/images/logo/favicon.svg"
            type="image/x-icon"
        />
        <link
            rel="shortcut icon"
            href="assets/images/logo/favicon.png"
            type="image/png"
        />

        <!-- additional style -->
        <style>
            /* Hide scrollbar for Chrome, Safari and Opera */
            .hide-scroll::-webkit-scrollbar {
                display: none;
            }

            /* Hide scrollbar for IE, Edge and Firefox */
            .hide-scroll {
                -ms-overflow-style: none; /* IE and Edge */
                scrollbar-width: none; /* Firefox */
            }
        </style>
    </head>

    <body>
        <div id="auth">
            <div class="row h-100">
                <div
                    class="col-lg-5 col-12 h-100 hide-scroll"
                    style="overflow-y: hidden"
                >
                    <div id="auth-left">
                        <div class="row justify-content-center text-center">
                            <!-- <div
                                class="col-md-12 col-sm-none mb-5"
                                style="
                                    object-fit: cover;
                                    width: 290px;
                                    height: 70px;
                                "
                            >
                                <a href="index.html"
                                    ><img
                                        height="100%"
                                        width="100%"
                                        
                                        src="https://images.squarespace-cdn.com/content/v1/626a42501862e66ba0315038/8d4a9555-220a-47b4-af03-0291181e2c87/INVENTORY_LOGO_RGB_2022.png"
                                        alt="Logo"
                                /></a>
                            </div> -->
                            <h1 class="auth-title mb-4">Log in.</h1>
                        </div>
                        <!-- <p class="auth-subtitle mb-5">
                            Log in with your data that you entered during
                            registration.
                        </p> -->

                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div
                                class="form-group position-relative has-icon-left mb-4"
                            >
                                <input
                                    type="text"
                                    class="form-control form-control-xl"
                                    name="email"
                                    placeholder="Email"
                                    :value="old('email')"
                                    required
                                    autofocus
                                />
                                <div class="form-control-icon">
                                    <i class="bi bi-envelope"></i>
                                </div>
                            </div>
                            <div
                                class="form-group position-relative has-icon-left mb-4"
                            >
                                <input
                                    type="password"
                                    name="password"
                                    class="form-control form-control-xl"
                                    placeholder="Password"
                                />
                                <div class="form-control-icon">
                                    <i class="bi bi-shield-lock"></i>
                                </div>
                            </div>

                            <!-- <div
                                class="form-check form-check-lg d-flex align-items-end"
                            >
                                <input
                                    class="form-check-input me-2"
                                    type="checkbox"
                                    value=""
                                    id="flexCheckDefault"
                                />
                                <label
                                    class="form-check-label text-gray-600"
                                    for="flexCheckDefault"
                                >
                                    Keep me logged in
                                </label>
                            </div> -->

                            <button
                                class="btn btn-primary btn-block btn-lg shadow-lg mt-2"
                                type="submit"
                            >
                                Log in
                            </button>
                        </form>
                        <div class="text-center mt-5 text-lg fs-4">
                            <p class="text-gray-600">
                                Don't have an account?
                                <a
                                    href="{{ route('register') }}"
                                    class="font-bold"
                                    >Sign up</a
                                >.
                            </p>
                            <!-- <p>
                                <a
                                    class="font-bold"
                                    href="auth-forgot-password.html"
                                    >Forgot password?</a
                                >.
                            </p> -->
                        </div>
                    </div>
                </div>
                <div
                    class="col-lg-7 d-none d-lg-block h-100"
                    style="object-fit: cover"
                >
                    <img
                        src="https://komerce.id/blog/wp-content/uploads/2021/08/ilustrasi-gudang.jpg"
                        width="100%"
                        height="100%"
                    />
                </div>
            </div>
        </div>
    </body>
</html>
