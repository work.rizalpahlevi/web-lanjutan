<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    public function index()
    {
        $barang = Barang::get();
        return view('pages.barang.index', compact(['barang']));
    }

    public function create()
    {

        return view('pages.barang.add');
    }

    public function edit($id)
    {
        $barang = Barang::find($id);

        return view('pages.barang.edit', compact(['barang']));
    }

    public function store(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'stock' => 'required|integer'
        ]);

        $barang = new Barang();
        $barang->name = $req->name;
        $barang->stock = $req->stock;
        $barang->notes = $req->notes;
        $barang->save();

        // run index
        if ($barang) {
            // return $this->index();
            return redirect()->back()->with(['result' => 'Data Berhasil Disimpan', 'type' => 'alert-success']);
        } else {
            return redirect()->back()->with(['result' => 'Data Gagal Disimpan', 'type' => 'alert-warning']);
        }
    }

    public function update(Request $req, $id)
    {
        $req->validate([
            'name' => 'required',
            'stock' => 'required|integer'
        ]);

        $barang = Barang::find($id);
        $barang->name = $req->name;
        $barang->stock = $req->stock;
        $barang->notes = $req->notes;
        $barang->save();

        // run index
        if ($barang) {
            // return $this->index();
            return redirect()->back()->with(['result' => 'Data Berhasil Diubah', 'type' => 'alert-success']);
        } else {
            return redirect()->back()->with(['result' => 'Data Gagal Diubah', 'type' => 'alert-warning']);
        }
    }

    public function delete($id)
    {
        
        $barang = Barang::find($id);
        $barang->delete();

        // run index
        if ($barang) {
            // return $this->index();
            return redirect()->back()->with(['result' => 'Data Berhasil Dihapus', 'type' => 'alert-success']);
        } else {
            return redirect()->back()->with(['result' => 'Data Gagal Dihapus', 'type' => 'alert-warning']);
        }
    }
}
