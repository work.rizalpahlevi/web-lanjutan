<?php

use App\Http\Controllers\BarangController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/old', function () {
    return view('dashboard-old');
});

Route::get('/', function () {
    return view('auth/login');
});


Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    
    // barang
    Route::get('/barang', [BarangController::class , 'index'] )->name('barang');
    Route::get('/barang/add', [BarangController::class , 'create'] )->name('add-barang');
    Route::post('/barang/store',[BarangController::class, 'store'])->name('store-barang');
    Route::get('/barang/edit/{id}', [BarangController::class , 'edit'] )->name('edit-barang');
    Route::put('/barang/update/{id}', [BarangController::class , 'update'] )->name('update-barang');
    Route::get('/barang/delete/{id}', [BarangController::class , 'delete'] )->name('delete-barang');
});
